import React from 'react'
import PropTypes from 'prop-types'
const Header = (props) => {
  return (
    <header >
    <div className='h1'>
    <div>
      <a href='#/'>
        <h1>E commerce websites</h1>
      </a>
    </div>
    <div>
      <a href='#/cart'>
        Cart{' '}
        {props.countCartItems ? (
          <button className='badge'>{props.countCartItems}</button>
        ) : (
          ''
        )}
      </a>{' '}
      <a href='#/signin'> SignIn</a>
    </div>
    </div>
  </header>
  )
}
Header.propTypes= {
  countCartItems:PropTypes.number,
  
  
}

export default Header