import React from 'react'
import PropTypes from 'prop-types'
const Basket = (props) => {
  const { cartItems, onAdd, onRemove } = props;
  const itemsPrice = cartItems.reduce((a, c) => a + c.qty * c.price, 0);
  const totalPrice = itemsPrice ;
  return (
    <div className='cart'>
    <h2>Cart Items</h2>
    <div>
      {cartItems.length === 0 && <div>Cart is empty</div>}
      {cartItems.map((item) => (
        <div key={item.id} className='cart2' >
          <div className='col-2'>{item.title}</div>
          <div className='col-2'>
          <img className='img1' style={{width:'50px', height:'50px', borderRadius:'50%'}} src={item.image} alt={item.name} ></img>
          <div>
          <button onClick={() => onRemove(item)} className="remove">
              -
              
            </button>{' '}
            <button onClick={() => onAdd(item)} className="add">
              +
            </button>
          </div>
          </div>

          <div className="col-2 text-right">
         
            {item.qty} x ${item.price.toFixed(2)}
          </div>
          <hr/>
        </div>
        
      ))}

      {cartItems.length !== 0 && (
        <>
          <hr></hr>
          <div >
            <div className="col-2">Items Price</div>
            <div className="col-1 text-right">${itemsPrice.toFixed(2)}</div>
          </div>
          <div >
            <div className="col-2">
              <strong>Total Price</strong>
            </div>
            <div className="col-1 text-right">
              <strong>${totalPrice.toFixed(2)}</strong>
            </div>
          </div>
          <hr />
          <div className="row">
            <button onClick={() => alert('Implement Checkout!')}>
              Checkout
            </button>
          </div>
        </>
      )}
    </div>
  </div>
  )
}

Basket.propTypes= {
  cartItems:PropTypes.array,
  onRemove:PropTypes.func,
  onAdd:PropTypes.func,
  
}

export default Basket