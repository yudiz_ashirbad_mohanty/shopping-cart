import React,{useState,useEffect} from 'react'
import Product from './Product';
import PropTypes from 'prop-types'
const url ='https://fakestoreapi.com/products'
const Main = (props) => {
const { onAdd } = props;
const [users,setUsers]=useState([]);
const [fillter,setFilter]=useState('');
const getUsers =async () =>{
 const response =await fetch(url);
 const users =await response.json();
 setUsers(users)
 console.log(users);
 
}

const searchText =(event)=>{
 setFilter(event.target.value);
}

let dataserch = users.filter(item =>{
 return Object.keys(item).some(key =>item[key].toString().toLowerCase().includes(fillter.toString().toLowerCase()))
})

useEffect(()=>{
 getUsers();
},[])


console.log();
  return (
    <main >
      
      <div className='search1' style={{margin:'50px'}}>
      
      <input type ="text"
      className="form-control"
      value={fillter}
      onChange={searchText.bind(this)}
      ></input>
      <label className="search">search</label>
    </div>
    <br/>
      <div className='Product1'>
      <div className='product2'>
        {dataserch.map((product) => (
          <Product key={product.id} product={product} onAdd={onAdd}></Product>
        ))}
      </div>
      </div>
    </main>
  )
}
Main.propTypes= {
  products:PropTypes.array,
  onAdd:PropTypes.func
}

export default Main