import React from 'react'
import PropTypes from 'prop-types'
const Product = (props) => {
  const { product, onAdd } = props;
  

  return (

    <div className='box'>
      <div className='card'>
        <h1>{ product.title}</h1>
        <img className='img1'src={product.image} alt={product.name} ></img>
        
        <p className='price'>${product.price}</p>
        <p className='category'>{product.category}</p>
        
        <p className='price'></p>
        <p><button onClick={() => onAdd(product)}>Add to Cart</button></p>
        </div>
    </div>
  
  )
}
Product.propTypes= {
  product:PropTypes.object,
  onAdd:PropTypes.func,
  
  
  
}

export default Product